import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class PinSetterTest {

    private PinSetter pinSetter;
    int firstShot = 0;
    String x = "";

    @BeforeEach
    public void setUp() {
        pinSetter = new PinSetter();
    }

    @Test
    public void canRegisterStrike() {
        int[] pinArray = {0,0,0,0,0,0,0,0,0,0};
        assertEquals("10", pinSetter.roll(1, pinArray));
    }

    @Test
    public void canRegisterGutter() {
        int[] pinArray = {1,1,1,1,1,1,1,1,1,1};
        assertEquals("0", pinSetter.roll(1, pinArray));
    }

    @Test
    public void canRegisterSpare() {
        int[] pinArray1 = {1,1,1,1,1,1,1,1,1,1};
        int[] pinArray2 = {0,0,0,0,0,0,0,0,0,0};
        assertEquals("0", pinSetter.roll(1, pinArray1));
        assertEquals("/", pinSetter.roll(2, pinArray2));
    }

    @Test
    public void canRegister9AndSpare() {
        int[] pinArray1 = {0,0,0,0,0,0,0,0,0,1};
        int[] pinArray2 = {0,0,0,0,0,0,0,0,0,0};
        assertEquals("9", pinSetter.roll(1, pinArray1));
        assertEquals("/", pinSetter.roll(2, pinArray2));
    }

    @Test
    public void canRegister7_10Split() {
        int[] pinArray = {0,0,0,0,0,0,1,0,0,1};
        x = pinSetter.roll(1, pinArray);
        firstShot = Integer.parseInt(x);
        assertEquals("S", pinSetter.splitDetect(firstShot, pinArray));
    }

    @Test
    public void pinLeaveIsWashout() {
        int[] pinArray = {1,1,0,1,0,0,0,0,0,1};
        x = pinSetter.roll(1, pinArray);
        firstShot = Integer.parseInt(x);
        assertEquals("n", pinSetter.splitDetect(firstShot, pinArray));
    }

    @Test
    public void canRegisterBigFour() {
        int[] pinArray = {0,0,0,1,0,1,1,0,0,1};
        x = pinSetter.roll(1, pinArray);
        firstShot = Integer.parseInt(x);
        assertEquals("S", pinSetter.splitDetect(firstShot, pinArray));
    }

    @Test
    public void canRegisterDoubleTrouble() {
        int[] pinArray = {0,0,1,1,0,1,1,0,1,1};
        int[] opposite = {0,1,0,1,0,1,1,1,0,1};
        x = pinSetter.roll(1, pinArray);
        firstShot = Integer.parseInt(x);
        assertEquals("S", pinSetter.splitDetect(firstShot, pinArray));
        assertEquals("S", pinSetter.splitDetect(firstShot, opposite));
    }

    @Test
    public void registersDoubleWoodIsNormalShot() {
        int[] pinArray = {0,1,0,0,0,0,0,1,0,0};
        int[] opposite = {0,0,1,0,0,0,0,0,1,0};
        x = pinSetter.roll(1, pinArray);
        firstShot = Integer.parseInt(x);
        assertEquals("n", pinSetter.splitDetect(firstShot, pinArray));
        assertEquals("n", pinSetter.splitDetect(firstShot, opposite));
    }

    @Test
    public void canRegister2_10Split() {
        int[] pinArray = {0,1,0,0,0,0,0,0,0,1};
        x = pinSetter.roll(1, pinArray);
        firstShot = Integer.parseInt(x);
        assertEquals("S", pinSetter.splitDetect(firstShot, pinArray));
    }

    @Test
    public void canRegister9PinSplit() {
        int[] pinArray = {0,1,1,1,1,1,1,1,1,1};
        x = pinSetter.roll(1, pinArray);
        firstShot = Integer.parseInt(x);
        assertEquals("S", pinSetter.splitDetect(firstShot, pinArray));
    }
}
