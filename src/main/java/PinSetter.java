public class PinSetter {

    public String roll(int shotCounter, int[] pinLeave) {
        int score = 10;
        for (int j : pinLeave) {
            score -= j;
        }
        String result = Integer.toString(score);
        if(shotCounter % 2 == 0 && score == 10)
            return "/";
        return result;
    }

    public String splitDetect(int firstShot, int[] pin) {
        int firstPin = 0;
        int remainder = 10 - firstShot;
        if(pin[0] != 0 || firstShot == 0 || firstShot >= 9) {
            return "n";
        }
        for(int i = 1; i < pin.length-1; i++) {
            if(pin[i] == 1) {
                firstPin = i;
                break;
            }
        }
        if(firstPin >= 6) {
            return "S";
        }
        else if(firstPin >= 3) {
            if (remainder - pin[firstPin] - pin[firstPin+3] - pin[firstPin+4] != 0) {
                return "S";
            }
        }
        else {
            if(pin[firstPin+2] == 1 && pin[firstPin+3] == 1) {
                remainder -= pin[firstPin] - pin[firstPin+2] - pin[firstPin+3];
                if(remainder - pin[firstPin+5] - pin[firstPin+6] - pin[firstPin+7] != 0) {
                    return "S";
                }
            }
            else if(pin[firstPin+2] == 1) {
                remainder -= pin[firstPin] - pin[firstPin+2];
                if(remainder - pin[firstPin+5] - pin[firstPin+6] != 0) {
                    return "S";
                }
            }
            else if(pin[firstPin+3] == 1) {
                remainder -= pin[firstPin] - pin[firstPin+3];
                if(remainder - pin[firstPin+6] - pin[firstPin+7] != 0) {
                    return "S";
                }
            }
            else if(pin[firstPin+2] == 0 && pin[firstPin+3] == 0) {
                if(pin[firstPin] + pin[firstPin+6] != remainder) {
                    return "S";
                }
            }
        }
        return "n";
    }
}
